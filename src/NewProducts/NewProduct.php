<?php

namespace Jahangir\NewProducts;

class NewProduct
{
    public $icon = null;
    public $title = null;
    public $content = null;

    public function getNewProducts()
    {
        return $new_products = [
            ['image' => '../front/img/product/p1.jpg', 'title' => '32GB Pendrive', 'price' => 120, 'btn_text' => 'Add to Bag', 'btn_url' => "#"],
            ['image' => '../front/img/product/p2.jpg', 'title' => 'Cool Air', 'price' => 120, 'btn_text' => 'Add to Bag', 'btn_url' => "#"],
            ['image' => '../front/img/product/p3.jpg', 'title' => 'Headphone', 'price' => 120, 'btn_text' => 'Add to Bag', 'btn_url' => "#"],
            ['image' => '../front/img/product/p4.jpg', 'title' => 'Mosque Spray', 'price' => 120, 'btn_text' => 'Add to Bag', 'btn_url' => "#"],
            ['image' => '../front/img/product/p1.jpg', 'title' => '32GB Pendrive Two', 'price' => 120, 'btn_text' => 'Add to Bag', 'btn_url' => "#"],
            ['image' => '../front/img/product/p2.jpg', 'title' => 'Cool Air Two', 'price' => 120, 'btn_text' => 'Add to Bag', 'btn_url' => "#"],
            ['image' => '../front/img/product/p4.jpg', 'title' => 'Mosque Spray Two', 'price' => 120, 'btn_text' => 'Add to Bag', 'btn_url' => "#"],
            ['image' => '../front/img/product/p3.jpg', 'title' => 'Headphone Two', 'price' => 120, 'btn_text' => 'Add to Bag', 'btn_url' => "#"],
           
        ];
    }
}
