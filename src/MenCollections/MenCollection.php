<?php

namespace Jahangir\MenCollections;

class MenCollection
{
    public $icon = null;
    public $title = null;
    public $content = null;

    public function getMenCollections()
    {
        return $men_collections = [
            ['image' => '../front/img/product/p1.jpg', 'title' => '32GB Pendrive', 'price' => 120, 'btn_text' => 'Add to Cart', 'btn_url' => "#"],
            ['image' => '../front/img/product/p2.jpg', 'title' => 'Cool Air', 'price' => 120, 'btn_text' => 'Add to Cart', 'btn_url' => "#"],
            ['image' => '../front/img/product/p3.jpg', 'title' => 'Headphone', 'price' => 120, 'btn_text' => 'Add to Cart', 'btn_url' => "#"],
            ['image' => '../front/img/product/p4.jpg', 'title' => 'Mosque Spray', 'price' => 120, 'btn_text' => 'Add to Cart', 'btn_url' => "#"],
          
           
        ];
    }
}
