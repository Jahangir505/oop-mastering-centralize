<?php

namespace Jahangir\Courses;

class Course
{
    public $image = null;
    public $title = null;
    public $sub_title = null;
    public $lessons = null;
    public $hours = null;
    public $price = 0;
    public $offer_price = 0;

    public function all()
    {
        return $courses = [
            ['title'=> 'Fashion Photography From Professional', 'sub_title' => 'Photography', 'image' => '../front/assets/img/products/product-1.jpg', 'lessons' => '5 lessons', 'hours' => '5h 12m', 'price' => 959, 'offer_price' => 415.99, 'best_seller' => true],
            ['title'=> 'Fashion Development From Professional', 'sub_title' => 'Development', 'image' => '../front/assets/img/products/product-2.jpg', 'lessons' => '9 lessons', 'hours' => '8h 12m', 'price' => 959, 'offer_price' => 415.99, 'best_seller' => false],
            ['title'=> 'Fashion Development From Professional', 'sub_title' => 'Development', 'image' => '../front/assets/img/products/product-3.jpg', 'lessons' => '15 lessons', 'hours' => '10h 12m', 'price' => 959, 'offer_price' => 415.99, 'best_seller' => false],
            ['title'=> 'Fashion Development From Professional', 'sub_title' => 'Development', 'image' => '../front/assets/img/products/product-4.jpg', 'lessons' => '6 lessons', 'hours' => '4h 12m', 'price' => 959, 'offer_price' => 415.99, 'best_seller' => true],
            ['title'=> 'Fashion Development From Professional', 'sub_title' => 'Development', 'image' => '../front/assets/img/products/product-4.jpg', 'lessons' => '6 lessons', 'hours' => '4h 12m', 'price' => 959, 'offer_price' => 415.99, 'best_seller' => false],
            ['title'=> 'Fashion Development From Professional', 'sub_title' => 'Development', 'image' => '../front/assets/img/products/product-4.jpg', 'lessons' => '6 lessons', 'hours' => '4h 12m', 'price' => 959, 'offer_price' => 415.99, 'best_seller' => false],
        ];
    }
}
