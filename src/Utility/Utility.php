<?php

namespace Jahangir\Utility;

class Utility
{
    public static function debug($data)
    {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        echo "<hr />";
    }

    public static function d($data)
    {
        self::debug($data);
    }

    public static function dd($data)
    {
        self::debug($data);
        die(__FILE__.__LINE__);
    }

    public static function redirect($url)
    {
        header('location:'.$url);
    }
}
