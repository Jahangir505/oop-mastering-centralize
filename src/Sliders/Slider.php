<?php

namespace Jahangir\Sliders;

class Slider
{
    public $title = null;
    public $image = null;
    public $content = null;

    public function getAllSlider()
    {
        return $sliders = [
            ['title'=> 'Fashion Photography From Professional', 'image' => '../front/img/slider/slide-1.jpg'],
            ['title'=> 'Fashion Photography From Professional', 'image' => '../front/img/slider/slider-2.jpg'],
            ['title'=> 'Fashion Photography From Professional', 'image' => '../front/img/slider/slider-3.jpg'],
        ];
    }
}
