<?php include_once($_SERVER['DOCUMENT_ROOT']. "/HomeWork/front/includes/header.php"); ?>
    <section class="pb-5">
      <div class="container">
        <div class="heading">
          <h1 class="py-5 text-center bg-info">Product List</h1>
        </div>
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/HomeWork/front/partials/products.php"); ?>
      </div>
    </section>

<?php include_once($_SERVER['DOCUMENT_ROOT']."/HomeWork/front/includes/footer.php"); ?>