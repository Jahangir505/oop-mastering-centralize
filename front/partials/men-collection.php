<div class="row row-cols-1 row-cols-md-3 g-4 text-center">
          <?php foreach ($men_collections as $men): ?>
          <div class="col-md-3">
            <div class="card">
              <img
                src="<?= $men['image']; ?>"
                class="card-img-top"
                alt="..."
                height="200"
              />
              <div class="card-body border">
                <h5 class="card-title"><?= $men['title']; ?></h5>
                <p class="card-text">$<?= $men['price']; ?></p>
                <a href="product-list.php" class="btn btn-info"><?= $men['btn_text']; ?></a>
              </div>
            </div>
          </div>
            <?php endforeach; ?>
          

        </div>