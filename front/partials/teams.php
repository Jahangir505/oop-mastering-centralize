<div class="mx-n3 mx-md-n4" data-flickity='{"pageDots": true, "prevNextButtons": false, "cellAlign": "left", "wrapAround": true, "imagesLoaded": true}'>
                <?php foreach ($instructors as $instructor): ?>
                <div class="col-6 col-md-4 col-lg-3 text-center py-5 text-md-left px-3 px-md-4" data-aos="fade-up" data-aos-delay="50">
                    <div class="card rounded-lg border shadow p-2 lift">
                        <!-- Image -->
                        <div class="card-zoom position-relative" style="max-width: 250px;">
                            <div class="card-float card-hover right-0 left-0 bottom-0 mb-4">
                                <ul class="nav mx-n4 justify-content-center">
                                    <?php foreach ($instructor['social_media'] as $media): ?>
                                    <li class="nav-item px-4">
                                        <a href="#" class="d-block text-white">
                                            <i class="<?= $media['icon'] ?>"></i>
                                        </a>
                                    </li>
                                    <?php endforeach; ?>
                                    
                                </ul>
                            </div>

                            <a href="./instructors-single.html" class="card-img sk-thumbnail img-ratio-4 card-hover-overlay d-block"><img class="rounded shadow-light-lg img-fluid" src="<?= $instructor['image'] ?>" alt="..."></a>
                        </div>

                        <!-- Footer -->
                        <div class="card-footer px-3 pt-4 pb-1">
                            <a href="#" class="d-block"><h5 class="mb-0"><?= $instructor['name'] ?></h5></a>
                            <span class="font-size-d-sm"><?= $instructor['designation'] ?></span>
                        </div>
                    </div>
                </div>

                <?php endforeach; ?>
               
            </div>