<div class="row row-cols-1 row-cols-md-3 g-4 text-center">
          <?php foreach ($new_collections as $collection): ?>
          <div class="col-md-3">
            <div class="card">
              <img
                src="<?= $collection['image']; ?>"
                class="card-img-top"
                alt="..."
                height="200"
              />
              <div class="card-body border">
                <h5 class="card-title"><?= $collection['title']; ?></h5>
                <p class="card-text">$<?= $collection['price']; ?></p>
                <a href="product-list.php" class="btn btn-info"><?= $collection['btn_text']; ?></a>
              </div>
            </div>
          </div>
          <?php endforeach; ?>
          
        </div>