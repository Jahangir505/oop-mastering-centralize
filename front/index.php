

<?php include_once($_SERVER['DOCUMENT_ROOT']. "/HomeWork/front/includes/header.php"); ?>
<?php
  use Jahangir\Database;
  use Jahangir\Utility\Utility;

  $product = new Database();

  $data = $product->showData("sliders");

  Utility::dd($data);
  
?>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <?php include_once($_SERVER['DOCUMENT_ROOT']."/HomeWork/front/partials/slider.php"); ?>
        </div>
      </div>
    </div>

    <div class="container">
      <h1 class="py-5">| Featured Product</h1>

      <div class="row text-center">
        <div class="col-md-4">
          <div
            class="card bg-dark text-white text-center d-flex align-items-center"
            style="height: 100%"
          >
            <img
              src="img/section/left.png"
              class="card-img"
              alt="..."
              height="100%"
            />
            <div class="card-img-overlay">
              <div class="content">
                <h5 class="card-title">SALE</h5>
                <p class="card-text">50% OFF ON NEW ARRIVAL.</p>
                <a href="product-list.html" class="btn btn-danger">SHOP NOW</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-8">
          <div class="row">
            <div class="col-md-12 pb-4">
              <div class="card bg-dark text-white text-center">
                <img
                  src="img/section/fashion4.jpeg"
                  class="card-img"
                  alt="..."
                  height="300"
                />
                <div class="card-img-overlay">
                  <div class="content">
                    <h5 class="card-title">MEN'S</h5>
                    <p class="card-text">40% OFF ON NEW ARRIVAL.</p>
                    <a href="product-list.html" class="btn btn-danger"
                      >SHOP NOW</a
                    >
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card bg-dark text-white text-center">
                <img
                  src="img/section/fashion.jpg"
                  class="card-img"
                  alt="..."
                  height="300"
                />
                <div class="card-img-overlay">
                  <div class="content">
                    <h5 class="card-title">KID'S</h5>
                    <p class="card-text">50% OFF ON NEW ARRIVAL.</p>
                    <a href="product-list.html" class="btn btn-danger"
                      >SHOP NOW</a
                    >
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card bg-dark text-white text-center">
                <img
                  src="img/section/fashion2.jpg"
                  class="card-img"
                  alt="..."
                  height="300"
                />
                <div class="card-img-overlay">
                  <div class="content">
                    <h5 class="card-title">MEN'S</h5>
                    <p class="card-text">50% OFF ON NEW ARRIVAL.</p>
                    <a href="product-list.html" class="btn btn-danger"
                      >SHOP NOW</a
                    >
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <section class="pt-5 pb-5">
      <div class="container">
        <div class="heading py-3">
          <h2>| NEW ARRIVAL</h2>
        </div>
          <?php include_once($_SERVER['DOCUMENT_ROOT']."/HomeWork/front/partials/new-product.php"); ?>
      </div>
    </section>

    <section class="pt-5 pb-5">
      <div class="container">
        <div class="heading py-3">
          <h2>| MEN'S COLLECTION</h2>
        </div>
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/HomeWork/front/partials/men-collection.php"); ?>
      </div>
    </section>

   
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/HomeWork/front/includes/footer.php"); ?>