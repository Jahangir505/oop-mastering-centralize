
<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/HomeWork/config.php');
use Jahangir\Sliders\Slider;
use Jahangir\NewProducts\NewProduct;
use Jahangir\MenCollections\MenCollection;

$new_product = new NewProduct();
$men_collection = new MenCollection();
$slider = new Slider();

$sliders = $slider->getAllSlider();
$new_collections = $new_product->getNewProducts();
$men_collections = $men_collection->getMenCollections();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>PHP-OOP-E-commerce</title>
    <link
      href="css/bootstrap.min.css"
      rel="stylesheet"
      crossorigin="anonymous"
    />
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/fontawesome.min.css"
      integrity="sha512-xX2rYBFJSj86W54Fyv1de80DWBq7zYLn2z0I9bIhQG+rxIF6XVJUpdGnsNHWRa6AvP89vtFupEPDP8eZAtu9qA=="
      crossorigin="anonymous"
      referrerpolicy="no-referrer"
    />

    <link rel="stylesheet" href="css/main.css" />
  </head>
  <body>
    <header>
      <div class="container">
        <div class="topbar bg-info d-flex justify-content-between">
          <div class="left-bar">
            <ul class="d-flex align-items-center">
              <li>
                <small><b>Mobile.</b> 01778175444</small>
              </li>
              <li>
                <small><b>Email.</b> example@gmail.com</small>
              </li>
            </ul>
          </div>
          <ul class="nav">
            <li class="nav-item">
              <a class="nav-link" href="login.html">Login</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Signup</a>
            </li>
            <li class="nav-item">
              <a class="nav-link"
                ><i class="fa-brands fa-facebook-square"></i
              ></a>
            </li>
          </ul>
        </div>
        <nav class="navbar navbar-expand-lg bg-dark navbar-dark">
          <div class="container-fluid">
            <a class="navbar-brand border border-light px-4" href="#"
              ><h2><em>Logo</em></h2></a
            >
            <button
              class="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                  <a class="nav-link active" aria-current="page" href="#"
                    >Home</a
                  >
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">About</a>
                </li>
                <li class="nav-item dropdown">
                  <a
                    class="nav-link dropdown-toggle"
                    href="#"
                    id="navbarDropdown"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    Service
                  </a>
                  <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="#">Service One</a></li>
                    <li>
                      <a class="dropdown-item" href="#">Service Two</a>
                    </li>
                    <li><hr class="dropdown-divider" /></li>
                    <li>
                      <a class="dropdown-item" href="#">Go to </a>
                    </li>
                  </ul>
                </li>
                <li class="nav-item">
                  <a class="nav-link">Blog</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link">Contact</a>
                </li>
              </ul>
              <form class="d-flex" role="search">
                <input
                  class="form-control me-2"
                  type="search"
                  placeholder="Search"
                  aria-label="Search"
                />
                <button class="btn btn-outline-success" type="submit">
                  Search
                </button>
              </form>
              <div class="d-flex align-items-center px-4">
                <img src="img/cart.jpg" alt="" width="50" />
                <span class="text-white">10.00</span>
              </div>
            </div>
          </div>
        </nav>
      </div>
    </header>
