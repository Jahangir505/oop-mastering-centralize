

    <section class="subscribe">
      <div class="container py-5">
        <div class="subscribe-content text-center">
          <h2>Get Update Into</h2>
          <p>
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Repellat
            ullam culpa dignissimos neque ipsum alias eum, beatae vel delectus
            commodi
          </p>
          <div class="input-group mb-3">
            <input
              type="email"
              class="form-control"
              placeholder="Type your email..."
              aria-label="Type your email"
              aria-describedby="button-addon2"
            />
            <button class="btn" type="button" id="button-addon2">
              Subscribe
            </button>
          </div>
        </div>
      </div>
    </section>
<footer class="bg-dark">
      <div class="container py-5">
        <div class="row">
          <div class="col-md-4 text-white">
            <h2><em class="border px-4">Logo</em></h2>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore
              nihil voluptas doloribus debitis veniam quos, accusantium!
            </p>
          </div>
          <div class="col-md-2">
            <h5 class="text-white">Menu Item</h5>
            <ul class="foot-menu">
              <li class="">Privachy & Policy</li>
              <li class="">Terms</li>
              <li class="">FAQ</li>
              <li class="">Carrer</li>
            </ul>
          </div>
          <div class="col-md-2">
            <h5 class="text-white">Menu Item</h5>
            <ul class="foot-menu">
              <li class="">About</li>
              <li class="">Blog</li>
              <li class="">Categories</li>
              <li class="">Brands</li>
            </ul>
          </div>
          <div class="col-md-2">
            <h5 class="text-white">Menu Item</h5>
            <ul class="foot-menu">
              <li class="">An item</li>
              <li class="">A second item</li>
              <li class="">A third item</li>
            </ul>
          </div>
          <div class="col-md-2">
            <h5 class="text-white">Menu Item</h5>
            <ul class="foot-menu" style="background: transparent">
              <li class="">An item</li>
              <li class="">A second item</li>
              <li class="">A third item</li>
              <li class="">A fourth item</li>
            </ul>
          </div>
        </div>
      </div>
    </footer>

    <script>
      // external js: masonry.pkgd.js

      $(".grid").masonry({
        itemSelector: ".grid-item",
        columnWidth: ".grid-sizer",
        percentPosition: true,
      });
    </script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/js/fontawesome.min.js"
      integrity="sha512-5qbIAL4qJ/FSsWfIq5Pd0qbqoZpk5NcUVeAAREV2Li4EKzyJDEGlADHhHOSSCw0tHP7z3Q4hNHJXa81P92borQ=="
      crossorigin="anonymous"
      referrerpolicy="no-referrer"
    ></script>
    <script src="js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
  </body>
</html>
