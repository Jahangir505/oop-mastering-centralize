<section class="products">
      <div class="container">
        <h2 class="h2 upp align-center"> Hello </h2>
        <hr class="offset-lg">

        <div class="row">
            <?php
       
           
            foreach ($products as $product):
            // echo $counter;
            ?>

          <div class="col-sm-6 col-md-3 product">
            <div class="body">
              <a href="#favorites" class="favorites" data-favorite="inactive"><i class="ion-ios-heart-outline"></i></a>
              <a href="./"><img src="<?=$product['picture'] ?>" alt="Microsoft Surface Studio"/></a>

              <div class="content align-center">
                <p class="price"><?=$product['price'] ?></p>
                <h2 class="h3"><?=$product['captiontitle'] ?></h2>
                <hr class="offset-sm">

                <button class="btn btn-link"> <i class="ion-android-open"></i> <?=$product['button_tag'] ?></button>
                <button class="btn btn-primary btn-sm rounded"> <i class="ion-bag"></i> Add to cart</button>
              </div>
            </div>
          </div>
          <?php
           
            endforeach;
            ?>
         

        </div>

        <div class="align-right align-center-xs">
          <hr class="offset-sm">
          <a href="./store/"> <h5 class="upp">View all desktops </h5> </a>
        </div>
      </div>
    </section>
